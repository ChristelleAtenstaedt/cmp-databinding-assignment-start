import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css'],
})
export class GameControlComponent implements OnInit {
  constructor() {}
  @Output() numberIncrement = new EventEmitter<number>();
  interval: any;
  numberToIncrement = 0;
  ngOnInit(): void {}

  startGame() {
    // Start emitting an event with an incrementing number every second
    this.interval = setInterval(() => {
      this.numberIncrement.emit(this.numberToIncrement++);
    }, 1000);
  }

  stopGame() {
    // Stop emitting events
    clearInterval(this.interval);
    console.log('Game stopped!');
  }
}
